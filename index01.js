const { response } = require('express');
let express = require ('express');
const { request } = require('http');
let app = express();
const port = 8000;

// recurso principal
app.get ('/', (request, response)=>{
    response.send("{message: método get principal}");
}); // '/' é a raiz

// recurso funcionario
app.get ('/funcionario', (request, response)=>{
    response.send("{message: método get funcionario}");
});
//habilitando o serviço na porta 8000

app.listen(port, function(){
    console.log("Projeto rodando na porta: " + port);
});